from django import forms
from .models import Solicitud
from django.utils.translation import ugettext_lazy as _
from django.forms import widgets
from django_countries.fields import CountryField
import datetime
import calendar
from datetime import timedelta
from django.utils.safestring import mark_safe


class CrearSolicitudForm(forms.ModelForm):

    class Meta:
        model = Solicitud
        fields = ['rut', 'email', 'nombre', 'carrera',
                  'started_year', 'telefono', 'celular',
                  'numero_practica', 'nombre_empresa', 'rubro_empresa',
                  'direccion_empresa', 'website_empresa', 'ciudad_empresa',
                  'region_empresa', 'pais', 'contacto', 'cargo_contacto',
                  'email_contacto', 'telefono_contacto', 'fax_contacto',
                  'trabaja_empresa', 'empleados_empresa', 'empleados_IT',
                  'descripcion', 'inicio_practica', 'termino_practica',
                  'horas_diarias', 'dias_trabajo', 'objetivos_generales',
                  'actividades_realizar']

    def __init__(self, *args, **kwargs):
        super(CrearSolicitudForm, self).__init__(*args, **kwargs)
        for field in self.fields.values():
            field.error_messages = {'required':'El campo {fieldname} es requerido'.format(
                fieldname=field.label)}


    # def clean_rut(self):
    #     rut = self.cleaned_data['rut']
    #     if len(rut) < 8:
    #         raise forms.ValidationError("Rut inválido.")

    #     # Always return a value to use as the new cleaned data, even if
    #     # this method didn't change it.
    #     return rut

    def clean_horas_diarias(self):
        horas = self.cleaned_data['horas_diarias']
        if horas < 1 or horas > 12:
            raise forms.ValidationError(
                    "La cantidad de horas no se encuentra dentro de los parámetros establecidos")
        return horas

    def clean_started_year(self):
        year = self.cleaned_data['started_year']
        now = datetime.datetime.now()
        if year > now.year or year < now.year-12:
            raise forms.ValidationError(
                    "El año de ingreso no se encuentra dentro de los parámetros establecidos ({past} - {current})".format(past=now.year-12,current=now.year))
        return year

    def clean_dias_trabajo(self):
        dias = self.cleaned_data['dias_trabajo']
        dias_str = ', '.join(dias)
        return dias_str

    def clean_celular(self):
        celular = self.cleaned_data['celular']
        digitos = celular/10**7
        if digitos < 1 or digitos >= 10:
            raise forms.ValidationError("La cantidad de digitos del celular deben ser 8")
        return celular

    def clean_telefono(self):
        telefono = self.cleaned_data['telefono']
        digitos = telefono/10**6
        if digitos < 1 or digitos >= 10:
            raise forms.ValidationError("La cantidad de digitos del teléfono deben ser 7")
        return telefono

    def clean_telefono_contacto(self):
        telefono_contacto = self.cleaned_data['telefono_contacto']
        digitos = telefono_contacto/10**6
        if digitos < 1 or digitos >= 100:
            raise forms.ValidationError("La cantidad de digitos del teléfono deben ser 7 (teléfono fijo) ó 8 (celular)")
        return telefono_contacto

    def weekday_count(self, start, end):
        start_date  = start
        end_date    = end
        week        = {}
        for i in range((end_date - start_date).days):
          day       = calendar.day_name[(start_date + datetime.timedelta(days=i+1)).weekday()]
          week[day] = week[day] + 1 if day in week else 1
        if 'Monday' not in week:
            week['Monday'] = 0
        if 'Tuesday' not in week:
            week['Tuesday'] = 0
        if 'Wednesday' not in week:
            week['Wednesday'] = 0
        if 'Thursday' not in week:
            week['Thursday'] = 0
        if 'Friday' not in week:
            week['Friday'] = 0
        if 'Saturday' not in week:
            week['Saturday'] = 0
        if 'Sunday' not in week:
            week['Sunday'] = 0
        return week

    def hours_count(self,dias_trabajo,horas_diarias,week):
        count = 0
        if 'LU' in dias_trabajo:
          count += horas_diarias * week['Monday']
        if 'MA' in dias_trabajo:
          count += horas_diarias * week['Tuesday']
        if 'MI' in dias_trabajo:
          count += horas_diarias * week['Wednesday']
        if 'JU' in dias_trabajo:
          count += horas_diarias * week['Thursday']
        if 'VI' in dias_trabajo:
          count += horas_diarias * week['Friday']
        if 'SA' in dias_trabajo:
          count += horas_diarias * week['Saturday']
        if 'DO' in dias_trabajo:
          count += horas_diarias * week['Sunday']
        return count

    def clean(self):
        cleaned_data = super().clean()
        if self.errors:
            return cleaned_data
        inicio_practica = cleaned_data.get("inicio_practica")
        termino_practica = cleaned_data.get("termino_practica")
        week = self.weekday_count(inicio_practica - timedelta(days=1),termino_practica)
        horas_diarias = cleaned_data['horas_diarias']
        dias_trabajo = cleaned_data['dias_trabajo']
        horas_totales = self.hours_count(dias_trabajo, horas_diarias, week)
        duracion = termino_practica - inicio_practica

        empleados_empresa = cleaned_data.get("empleados_empresa")
        empleados_IT = cleaned_data.get("empleados_IT")
        numero_practica = cleaned_data.get("numero_practica")

        if horas_totales < 200:
            raise forms.ValidationError("La práctica debe tener una duración mínima de 200 horas.")

        if duracion.days < 45:
            raise forms.ValidationError("La práctica debe tener una duración mínima de 45 días.")

        if numero_practica == 'Practica I':
            if empleados_IT < 5:
                raise forms.ValidationError("La práctica debe tener al menos 5 empleados del área IT.")

        if numero_practica == 'Practica II':
            if empleados_IT < 10 and empleados_empresa < 30:
                raise forms.ValidationError(
                    "La práctica debe tener al menos 10 empleados del área IT y/o 30 empleados en total.")



    NUMERO_PRACTICA = (
        ('Practica I', 'Practica I'),
        ('Practica II', 'Practica II'),
    )

    OPTIONS = (
        ("LU", "Lunes"),
        ("MA", "Martes"),
        ("MI", "Miercoles"),
        ("JU", "Jueves"),
        ("VI", "Viernes"),
        ("SA", "Sabado"),
        ("DO", "Domingo"),
    )
    rut = forms.CharField(label=_("<span class='error'>RUT</span>"),
                           widget=forms.TextInput
                           (attrs={'class': 'form-control'}))

    email = forms.EmailField(label=_("Correo"), widget=forms.EmailInput
                          (attrs={'class': 'form-control'}))
    nombre = forms.CharField(label=_("Nombre"),
                              widget=forms.TextInput
                              (attrs={'class': 'form-control'}))
    carrera = forms.CharField(label=_("Carrera"),
                                widget=forms.TextInput
                                (attrs={'class': 'form-control'}))
    started_year = forms.IntegerField(widget=forms.NumberInput(
      attrs={'class': 'validate w-85 form-control form-control-lg spaceinputlabel',
             'placeholder': 'Año ingreso'}))
    telefono = forms.IntegerField(widget=forms.NumberInput(
      attrs={'class': 'validate w-85 form-control form-control-lg spaceinputlabel',
             'placeholder': 'Teléfono'}))
    celular = forms.IntegerField(widget=forms.NumberInput(
      attrs={'class': 'validate w-85 form-control form-control-lg spaceinputlabel',
             'placeholder': 'Celular'}))
    numero_practica = forms.ChoiceField(choices=NUMERO_PRACTICA,widget=forms.Select(
      attrs={'class': 'browser-default custom-select ml-2'}))
    nombre_empresa = forms.CharField(widget=forms.TextInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'placeholder': 'Nombre de la empresa'}))
    rubro_empresa = forms.CharField(widget=forms.TextInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'placeholder': 'Rubro de la empresa'}))
    direccion_empresa = forms.CharField(widget=forms.TextInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'placeholder': 'Dirección de la empresa   Número'}))
    website_empresa = forms.CharField(widget=forms.TextInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'placeholder': 'Página web de la empresa'}))
    ciudad_empresa = forms.CharField(widget=forms.TextInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'placeholder': 'Ciudad'}))
    region_empresa = forms.CharField(widget=forms.TextInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'placeholder': 'Región'}))
    pais = CountryField(blank_label='Seleccionar país').formfield(widget=forms.Select
                                     (attrs={'class': 'form-control'}))
    contacto = forms.CharField(widget=forms.TextInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'placeholder': 'Nombre del contacto'}))
    cargo_contacto = forms.CharField(widget=forms.TextInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'placeholder': 'Cargo del contacto'}))
    email_contacto = forms.EmailField(widget=forms.EmailInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'placeholder': 'E-mail del contacto'}))
    telefono_contacto = forms.IntegerField(widget=forms.NumberInput(
      attrs={'class': 'validate w-90 form-control form-control-lg spaceinputlabel',
             'placeholder': 'Teléfono'}))
    fax_contacto = forms.CharField(required=False, widget=forms.TextInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'placeholder': 'FAX Empresa'}))
    trabaja_empresa = forms.BooleanField(required=False, widget=forms.CheckboxInput(
      attrs={'class': 'checkbox pull-right',
             'style': 'width: 20px; height: 20px;'}))
    empleados_empresa = forms.IntegerField(widget=forms.NumberInput(
      attrs={'class': 'validate form-control form-control-textoseguido ml-n4'}))
    empleados_IT = forms.IntegerField(widget=forms.NumberInput(
      attrs={'class': 'validate form-control form-control-textoseguido ml-n5'}))
    descripcion = forms.CharField(widget=forms.Textarea(
      attrs={'class': 'validate form-control form-control-lg',
             'rows': '3'}))
    inicio_practica = forms.DateField(
        widget=forms.DateInput(format='%d/%m/%Y',
                               attrs={'class': 'form-control datepicker',
                                      'onkeydown': 'return false'}),
        input_formats=('%d/%m/%Y', )
        )
    termino_practica = forms.DateField(
        widget=forms.DateInput(format='%d/%m/%Y',
                               attrs={'class': 'form-control datepicker',
                                      'onkeydown': 'return false'}),
        input_formats=('%d/%m/%Y', )
        )
    horas_diarias = forms.IntegerField(widget=forms.NumberInput(
      attrs={'class': 'validate form-control form-control-lg spaceinputlabel',
             'min': '0'}))
    dias_trabajo = forms.MultipleChoiceField(label=("<span class='error'>DÍAS A TRABAJAR</span>"), widget=forms.CheckboxSelectMultiple,
                                          choices=OPTIONS)
    objetivos_generales = forms.CharField(widget=forms.Textarea(
      attrs={'class': 'validate form-control form-control-lg',
             'rows': '3'}))
    actividades_realizar = forms.CharField(widget=forms.Textarea(
      attrs={'class': 'validate form-control form-control-lg',
             'rows': '3'}))