from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_countries.fields import CountryField

class Solicitud(models.Model):

    rut = models.CharField(verbose_name="rut", max_length=10, unique=True)
    email = models.EmailField(_('email'))
    nombre = models.CharField(verbose_name="nombre", max_length=50)
    carrera = models.CharField(verbose_name="carrera", max_length=60)
    malla = models.CharField(verbose_name="malla", max_length=60, null=True, blank=True)
    started_year = models.IntegerField(verbose_name="started_year")
    telefono = models.IntegerField(verbose_name="telefono")
    celular = models.IntegerField(verbose_name="celular")
    NUMERO_PRACTICA = (
        ('Practica I', 'Practica I'),
        ('Practica II', 'Practica II'),
    )
    numero_practica = models.CharField(max_length=30, choices=NUMERO_PRACTICA,
                                    verbose_name="numero practica")
    nombre_empresa = models.CharField(verbose_name="nombre empresa", max_length=50)
    rubro_empresa = models.CharField(verbose_name="rubro", max_length=30)
    direccion_empresa = models.CharField(verbose_name="direccion", max_length=50)
    website_empresa = models.CharField(verbose_name="website", max_length=30)
    ciudad_empresa = models.CharField(verbose_name="ciudad", max_length=30)
    region_empresa = models.CharField(verbose_name="region", max_length=30)
    pais = CountryField()
    contacto = models.CharField(verbose_name="contacto", max_length=30)
    cargo_contacto = models.CharField(verbose_name="cargo contacto", max_length=30)
    email_contacto = models.EmailField(_('email contacto'))
    telefono_contacto = models.IntegerField(verbose_name="telefono contacto")
    fax_contacto = models.CharField(verbose_name="fax", max_length=30, null=True, blank=True)
    trabaja_empresa = models.BooleanField(verbose_name="trabaja aqui",
                                 default=False, null=True)
    empleados_empresa = models.IntegerField(verbose_name="total empleados empresa")
    empleados_IT = models.IntegerField(verbose_name="total empleados IT")
    descripcion = models.TextField(verbose_name="descripcion empresa", blank=True,
                                   null=True)
    inicio_practica = models.DateField(verbose_name="inicio practica",
                                     max_length=100)
    termino_practica = models.DateField(verbose_name="termino practica",
                                     max_length=100)
    horas_diarias = models.CharField(verbose_name="horas_diarias", max_length=10)
    dias_trabajo = models.CharField(verbose_name="dias trabajo", max_length=100)
    objetivos_generales = models.TextField(verbose_name="objetivos generales")
    actividades_realizar = models.TextField(verbose_name="actividades a realizar")
    status_x = models.BooleanField(default=False, null=True)
    status_m = models.BooleanField(default=False, null=True)
    status_v = models.BooleanField(default=False, null=True)
    is_rejected = models.BooleanField(default=False, null=True, blank=True)
    rejected_reason = models.TextField(verbose_name="razon de rechazo", blank=True,
                                   null=True)

    def __str__(self):
        return self.nombre
