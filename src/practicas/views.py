from django.shortcuts import render, redirect
from django.views.generic.edit import UpdateView, CreateView
from django.views import View
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.contrib import messages
import sdk as ensena
import requests
from django.core import serializers
import json
from django.conf import settings
from .models import Solicitud
from .forms import CrearSolicitudForm
from .utils import Malla, CheckEIT, CheckVL
from django.shortcuts import get_object_or_404


ensena.SetApp(settings.ENSENA_KEY)

class Main(View):
    template_name = "create_solicitud.html"

    def get(self, request):
        token = request.GET['TOKEN']
        email = ensena.GetUser(token)['User']['Email']
        if CheckEIT(email) or CheckVL(email):
            return redirect('solicitudes', TOKEN=token)
        else:
            return redirect('create_solicitud', TOKEN=token)


class CreateSolicitud(View):
    template_name = "create_solicitud.html"

    def get(self, request, *args, **kwargs):
        try:
            token = kwargs['TOKEN']
        except:
            context = {"error": True}
            return render(request, self.template_name, context)
        try:
            user = ensena.GetUser(token)
        except:
            context = {"error": True}
            return render(request, self.template_name, context)

        if Solicitud.objects.filter(email=user['User']['Email']).exists():
            return redirect('solicitud_view', TOKEN=token)
        form = CrearSolicitudForm(request.GET)
        context = {"form": form, "user": user, "TOKEN": token}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        try:
            token = kwargs['TOKEN']
        except:
            context = {"error": True}
            return render(request, self.template_name, context)
        try:
            user = ensena.GetUser(token)
        except:
            context = {"error": True}
            return render(request, self.template_name, context)
        form = CrearSolicitudForm(request.POST)
        days = form['dias_trabajo'].value()
        if form.is_valid():
            solicitud = Solicitud(**form.cleaned_data)
            if solicitud.numero_practica == 'Practica I':
                solicitud.malla = Malla(user['User']['CoursesApproved'], user['User']['Courses'], 1)
            elif solicitud.numero_practica == 'Practica II':
                solicitud.malla = Malla(user['User']['CoursesApproved'], user['User']['Courses'], 2)
            solicitud.save()
        else:
            context = {'errors': form.errors.values , 'form': form, 'user': user, 'TOKEN': token,
                       'days': days}
            return render(request, self.template_name, context)
        return redirect('create_solicitud', TOKEN=token)


class Solicitudes(View):
    template_name = "solicitudes.html"

    def get(self, request, *args, **kwargs):
        try:
            token = kwargs['TOKEN']
        except:
            context = {"error": True}
            return render(request, self.template_name, context)
        try:
            email = ensena.GetUser(token)['User']['Email']
        except:
            context = {"error": True}
            return render(request, self.template_name, context)

        if CheckEIT(email):
            queryset = Solicitud.objects.filter(status_m=False, is_rejected=False)

        elif CheckVL(email):
            queryset = Solicitud.objects.filter(status_v=False, is_rejected=False)
        else:
            context = {"error": True}
            return render(request, self.template_name, context)
        context = {"solicitudes": queryset, "TOKEN": token}
        return render(request, self.template_name, context)


class ApproveSolicitud(View):
    template_name = 'approve_solicitud.html'

    def get(self, request, *args, **kwargs):
        pk = kwargs['ID']
        try:
            token = kwargs['TOKEN']
        except:
            return redirect('solicitudes_error')
        solicitud = get_object_or_404(Solicitud, pk=pk)
        context = {"solicitud": solicitud, "TOKEN": token}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        pk = kwargs['ID']
        try:
            token = kwargs['TOKEN']
        except:
            return redirect('solicitudes_error')
        try:
            email = ensena.GetUser(token)['User']['Email']
        except:
            return redirect('solicitudes', TOKEN=token)
        solicitud = get_object_or_404(Solicitud, pk=pk)
        if CheckEIT(email):
            solicitud.status_m = True
            solicitud.save()
            messages.success(request, "Solicitud aprobada correctamente")
            return redirect('solicitudes', TOKEN=token)

        elif CheckVL(email):
            solicitud.status_v = True
            solicitud.save()
            messages.success(request, "Solicitud aprobada correctamente")
            return redirect('solicitudes', TOKEN=token)

        else:
            return redirect('solicitudes', TOKEN=token)


class SolicitudView(View):
    template_name = "solicitud_view.html"

    def get(self, request, *args, **kwargs):
        try:
            token = kwargs['TOKEN']
        except:
            context = {"error": True}
            return render(request, self.template_name, context)
        try:
            user = ensena.GetUser(token)
        except:
            context = {"error": True}
            return render(request, self.template_name, context)
        if Solicitud.objects.filter(email=user['User']['Email']).exists():
            solicitud = Solicitud.objects.get(email=user['User']['Email'])
            context = {"solicitud": solicitud, "TOKEN": token}
            return render(request, self.template_name, context)
        else:
            return redirect('create_solicitud', TOKEN=token)


class RejectSolicitud(View):
    template_name = "reject_solicitud.html"

    def get(self, request, *args, **kwargs):
        pk = kwargs['ID']
        try:
            token = kwargs['TOKEN']
        except:
            return redirect('solicitudes_error')
        try:
            user = ensena.GetUser(token)
        except:
            return redirect('solicitudes', TOKEN=token)
        solicitud = get_object_or_404(Solicitud, pk=pk)
        context = {"solicitud": solicitud, "TOKEN": token}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        pk = kwargs['ID']
        try:
            token = kwargs['TOKEN']
        except:
            return redirect('solicitudes_error')
        try:
            email = ensena.GetUser(token)['User']['Email']
        except:
            return redirect('solicitudes', TOKEN=token)
        solicitud = get_object_or_404(Solicitud, pk=pk)

        if CheckEIT(email) or CheckVL(email):
            solicitud.rejected_reason = request.POST['rejected_reason']
            solicitud.status_v = False
            solicitud.status_m = False
            solicitud.is_rejected = True
            solicitud.save()
            messages.success(request, "Solicitud rechazada correctamente")
            return redirect('solicitudes', TOKEN=token)
        else:
            return redirect('solicitudes', TOKEN=token)


class SolicitudDetails(View):
    template_name = "solicitud_details.html"

    def get(self, request, *args, **kwargs):
        pk = kwargs['ID']
        try:
            token = kwargs['TOKEN']
        except:
            return redirect('solicitudes_error')
        try:
            user = ensena.GetUser(token)
        except:
            return redirect('solicitudes', TOKEN=token)

        solicitud = get_object_or_404(Solicitud, pk=pk)
        context = {"solicitud": solicitud, "TOKEN": token}
        return render(request, self.template_name, context)


class DeleteSolicitud(View):
    template_name = "delete_solicitud.html"

    def get(self, request, *args, **kwargs):
        try:
            token = kwargs['TOKEN']
        except:
            return redirect('solicitud_error')
        try:
            user = ensena.GetUser(token)
        except:
            return redirect('solicitud_error', TOKEN=token)
        context = {"TOKEN": token}
        return render(request, self.template_name, context)


    def post(self, request, *args, **kwargs):
        try:
            token = kwargs['TOKEN']
        except:
            return redirect('solicitud_error')
        try:
            email = ensena.GetUser(token)['User']['Email']
        except:
            return redirect('solicitud_error', TOKEN=token)
        solicitud = get_object_or_404(Solicitud, email=email)
        solicitud.delete()
        return redirect('create_solicitud', TOKEN=token)


class SolicitudCorrection(View):
    template_name = "solicitud_correction.html"

    def get(self, request, *args, **kwargs):
        try:
            token = kwargs['TOKEN']
        except:
            return redirect('solicitud_error')
        try:
            email = ensena.GetUser(token)['User']['Email']
        except:
            return redirect('solicitud_error', TOKEN=token)
        solicitud = get_object_or_404(Solicitud, email=email)
        days = solicitud.dias_trabajo.split(", ")
        print(days)
        if solicitud.is_rejected:
            form = CrearSolicitudForm(instance=solicitud)
            context = {"form": form, "TOKEN": token, "days": days}
            return render(request, self.template_name, context)
        else:
            return redirect('solicitud_view', TOKEN=token)


    def post(self, request, *args, **kwargs):
        try:
            token = kwargs['TOKEN']
        except:
            return redirect('solicitud_error')
        try:
            user = ensena.GetUser(token)['User']
        except:
            return redirect('solicitud_error', TOKEN=token)
        solicitud = get_object_or_404(Solicitud, email=user['Email'])
        solicitud.is_rejected = False
        solicitud.rejected_reason = None
        form = CrearSolicitudForm(request.POST, instance=solicitud)
        days = form['dias_trabajo'].value()
        if form.is_valid():
            form.save()
        else:
            context = {'errors': form.errors.values , 'form': form, 'TOKEN': token, 'days': days}
            return render(request, self.template_name, context)
        return redirect('solicitud_view', TOKEN=token)