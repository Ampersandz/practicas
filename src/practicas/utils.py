from django.conf import settings

pr_1 = ['ÁLGEBRA Y GEOMETRÍA', 'QUÍMICA', 'PROGRAMACIÓN', 'COMUNICACIÓN PARA LA INGENIERÍA', 'CÁLCULO I',
        'PROGRAMACIÓN AVANZADA', 'ÁLGEBRA LINEAL', 'CÁLCULO II', 'MECÁNICA',
        'ECUACIONES DIFERENCIALES', 'CÁLCULO III', 'CALOR Y ONDAS', 'ESTRUCTURAS DE DATOS', 'REDES DE DATOS',
        'PROBABILIDADES Y ESTADÍSTICAS', 'MÉTODOS NUMÉRICOS', 'DISEÑO Y ANÁLISIS DE ALGORITMOS', 'ELECTRICIDAD Y MAGNETISMO', 'INGLÉS I',
        'CFG', 'CFG']

pr_2 = ['ÁLGEBRA Y GEOMETRÍA', 'QUÍMICA', 'PROGRAMACIÓN', 'COMUNICACIÓN PARA LA INGENIERÍA', 'CÁLCULO I',
        'PROGRAMACIÓN AVANZADA', 'ÁLGEBRA LINEAL', 'CÁLCULO II', 'MECÁNICA',
        'ECUACIONES DIFERENCIALES', 'CÁLCULO III', 'CALOR Y ONDAS', 'ESTRUCTURAS DE DATOS', 'REDES DE DATOS',
        'PROBABILIDADES Y ESTADÍSTICAS', 'MÉTODOS NUMÉRICOS', 'DISEÑO Y ANÁLISIS DE ALGORITMOS', 'ELECTRICIDAD Y MAGNETISMO', 'INGLÉS I',
        'OPTIMIZACIÓN', 'ELECTRÓNICA Y ELECTROTECNIA', 'PROYECTO EN TICS I', 'BASES DE DATOS', 'INGLÉS II',
        'INTRODUCCIÓN  A LA ECONOMÍA', 'MODELOS ESTOCASTICOS Y SIMULACIÓN', 'SEÑALES Y SISTEMAS', 'SISTEMAS OPERATIVOS', 'SISTEMAS DIGITALES',
        'CONTABILIDAD Y COSTOS', 'INGENIERÍA DE SOFTWARE', 'COMUNICACIONES DIGITALES', 'DERECHO EN INGENIERÍA', 'ARQUITECTURA DE COMPUTADORES',
        'GESTIÓN ORGANIZACIÓNAL', 'CRIPTOGRAFÍA Y SEGURIDAD EN REDES', 'ARQUITECTURA DE SISTEMAS', 'PROYECTO EN TICS II',
        'CFG', 'CFG', 'CFG', 'CFG', 'PRÁCTICA PROFESIONAL 1']

EIT_list = settings.EIT.split(",")
VL_list = settings.VL.split(",")


def Malla(approved, courses, num):
    if num == 1:
        practica = pr_1.copy()

    elif num == 2:
        practica = pr_2.copy()


    for app in approved:
        if len(practica) == 0:
            return 'APROBADA'

        elif app['Name'] not in pr_2 and 'CIT' not in app['Code'] and 'ING' not in app['Code']:
            if 'CFG' in practica:
                practica.remove('CFG')

        elif app['Name'] in practica:
            practica.remove(app['Name'])

    if len(practica) == 1:
        if practica[0] == 'CFG':
            name = practica[0]
        else:
            name = practica[0]['Name']
        for course in courses:
            if len(practica) == 0:
                return 'CONDICIONAL:' + name

            elif course['Name'] not in pr_2 and 'CIT' not in course['Code'] and 'ING' not in app['Code']:
                practica.remove('CFG')

            elif course['Name'] in practica:
                practica.remove(course['Name'])
        return 'RECHAZADA'
    else:
        return 'RECHAZADA'


def CheckEIT(email):
    if email in EIT_list:
        return True
    else:
        return False


def CheckVL(email):
    if email in VL_list:
        return True
    else:
        return False