from django.conf.urls import url
from django.urls import path
from .views import (CreateSolicitud, Solicitudes, ApproveSolicitud, 
                    SolicitudView, RejectSolicitud, DeleteSolicitud,
                    SolicitudDetails, Main, SolicitudCorrection)

urlpatterns = [
    path('', Main.as_view(), name='main'),
    path('crear_solicitud/', CreateSolicitud.as_view(), name='crear_error'),
    path('solicitud/', SolicitudView.as_view(), name='solicitud_error'),
    path('solicitudes/', Solicitudes.as_view(), name='solicitudes_error'),
    path('solicitudes/<str:TOKEN>/', Solicitudes.as_view(), name='solicitudes'),
    path('solicitudes/aprobar/<int:ID>/<str:TOKEN>/', ApproveSolicitud.as_view(), name='approve_solicitud'),
    path('solicitudes/rechazar/<int:ID>/<str:TOKEN>/', RejectSolicitud.as_view(), name='reject_solicitud'),
    path('solicitudes/detalles/<int:ID>/<str:TOKEN>/', SolicitudDetails.as_view(), name='solicitud_details'),
    path('crear_solicitud/<str:TOKEN>/', CreateSolicitud.as_view(), name='create_solicitud'),
    path('solicitud/<str:TOKEN>/', SolicitudView.as_view(), name='solicitud_view'),
    path('solicitud/eliminar/<str:TOKEN>/', DeleteSolicitud.as_view(), name='delete_solicitud'),
    path('solicitud/recorregir/<str:TOKEN>/', SolicitudCorrection.as_view(), name='solicitud_correction'),
    ]