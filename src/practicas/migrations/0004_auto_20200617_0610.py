# Generated by Django 3.0.3 on 2020-06-17 06:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('practicas', '0003_auto_20200616_2332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solicitud',
            name='carrera',
            field=models.CharField(max_length=60, verbose_name='carrera'),
        ),
    ]
