$(document).ready(function() {
    $('#dataTable').DataTable( {
        "pageLength": 10,
        columnDefs: [
            {
                targets: [ 0, 1, 2, 3, 4 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ],
        language: {
            "decimal": "",
            "emptyTable": "No hay solicitudes actualmente",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ solicitudes",
            "infoEmpty": "Mostrando 0 to 0 of 0 solicitudes",
            "infoFiltered": "(Filtrado de _MAX_ total solicitudes)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ solicitudes",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "",
            "searchPlaceholder": "Buscar solicitudes",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },

        },
        "pagingType": "full_numbers"
    } );
} );
