# PASOS PARA LEVANTAR LOCALMENTE EL PROYECTO DE PRÁCTICAS.


1. Es necesario tener instalado [Docker](https://www.docker.com/products/docker-desktop)

2. Con el comando **Docker-compose up practicas** es posible levantar el proyecto, sin embargo, hay que tener en cuenta que se debe tener el archivo .env dentro de la carpeta src. Este archivo debe setear las variables DB_NAME, DB_HOST, DB_PASSWORD, DB_PORT, DB_USER, ENSENA_KEY . 

3. Al realizar lo anterior el proyecto ya estará corriendo localmente en el puerto 8000. Para simular el funcionamiento que se tendría en enseña, es necesario pasar en la URL el TOKEN personal, que cada integrante de la UDP tiene. Esto se haría poniendo en la URL localhost:8000?TOKEN=**TOKEN** . 