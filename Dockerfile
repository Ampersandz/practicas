FROM python:3.6
ENV PYTHONUNBUFFERED 1
ADD requirements/common.txt /
RUN pip install -r /common.txt; mkdir /src;
COPY ./src /src
WORKDIR /src